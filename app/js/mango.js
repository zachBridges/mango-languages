var input = "A B C\nB C E\nC G\nD A F\nE F\nF H\nF H";
var Dependencies = (function () {
    function Dependencies(inputDependencies) {
        //calculates the full set of dependencies for a group of items
        this.dependencies = {};
        this.parseInput(inputDependencies);
    }
    Dependencies.prototype.addDirect = function (item, dependencies) {
        this.dependencies[item] = dependencies;
    };
    Dependencies.prototype.getDirectDependencies = function (key) {
        return this.dependencies[key];
    };
    Dependencies.prototype.dependenciesFor = function (key) {
        //return full list of dependencies for an item
        var directDependencies = this.dependencies[key][0].split('');
        var listOfDependencies = [];
        var inheritedDependencies = [];
        /*
        let inheritedDependencies = this.getDirectDependencies(directDependencies[i]);
        // listOfDependencies.push(inheritedDependencies[0] );
        console.log('inheritedDependencies:',inheritedDependencies);
        // this.getDependencies(localDependencies[i]);
        */
        //inject local depdencies immediately
        for (var i = 0; i < directDependencies.length; i++) {
            if (directDependencies[i] === key) {
                return;
            }
            listOfDependencies.push(directDependencies[i]);
        }
        //get local dependencies' dependencies and push those in
        console.log(key, '\'s direct dependencies:', directDependencies);
        console.log(key, '\s full list of of dependencies:', listOfDependencies);
    };
    Dependencies.prototype.parseInput = function (input) {
        var inputArray = input.split('\n');
        for (var i = 0; i < inputArray.length; i++) {
            var key = inputArray[i].charAt(0);
            var keysDependencies = inputArray[i]
                .substring(1)
                .replace(/\s/g, '');
            this.addDirect(key, [keysDependencies]);
        }
    };
    Dependencies.prototype.printDependencies = function () {
        console.info('print:', this.dependencies);
    };
    return Dependencies;
}());
var dep = new Dependencies(input);
dep.dependenciesFor('A');
dep.printDependencies();
