var gulp = require('gulp');
var ts = require('gulp-typescript');
var tslint = require("gulp-tslint");

gulp.task('default', ['watch']);

gulp.task('typescript', function () {
  return gulp.src('src/**/*.ts')
    .pipe(tslint({
      formatter: 'verbose'
    }))
    .pipe(tslint.report())
    .pipe(ts({
      noImplicitAny: true,
      out: 'mango.js'
    }))
    .pipe(gulp.dest('app/js'))
});

gulp.task('watch', function(){
  gulp.watch('src/**/*.ts', ['typescript'])
})
