let input = "A B C\nB C E\nC G\nD A F\nE F\nF H\nF H";

interface DepdencyList{
  [key: string]: Array<string>;
}

class Dependencies{
  //calculates the full set of dependencies for a group of items
  dependencies: DepdencyList = {};

  constructor(inputDependencies: string){
    this.parseInput(inputDependencies);
  }

  addDirect(item:string, dependencies:Array<string>){
    this.dependencies[item] = dependencies;
  }

  getDirectDependencies(key:string):Array<string>{
    return this.dependencies[key];
  }

  dependenciesFor(key: string){
    //return full list of dependencies for an item
    let directDependencies: Array<string> = this.dependencies[key][0].split('');
    let listOfDependencies: Array<string> = [];
    let inheritedDependencies: Array<string> = [];

    /*
    let inheritedDependencies = this.getDirectDependencies(directDependencies[i]);
    // listOfDependencies.push(inheritedDependencies[0] );
    console.log('inheritedDependencies:',inheritedDependencies);
    // this.getDependencies(localDependencies[i]);
    */

    //inject local depdencies immediately
    for(let i = 0; i < directDependencies.length; i++){
      if(directDependencies[i] === key){return}
      listOfDependencies.push(directDependencies[i]);
    }
    //get local dependencies' dependencies and push those in



    console.log(key,'\'s direct dependencies:', directDependencies);
    console.log(key, '\s full list of of dependencies:', listOfDependencies);
  }

  parseInput(input: string){
    let inputArray = input.split('\n');
    for(let i = 0; i < inputArray.length; i++){
      let key: string = inputArray[i].charAt(0);
      let keysDependencies = inputArray[i]
        .substring(1)
        .replace(/\s/g,'');
      this.addDirect(key, [keysDependencies]);
    }
  }

  printDependencies():void{
    console.info('print:',this.dependencies);
  }
}

let dep = new Dependencies(input);
dep.dependenciesFor('A');
dep.printDependencies();
